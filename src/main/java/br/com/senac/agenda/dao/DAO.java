package br.com.senac.agenda.dao;

import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.Query;

public abstract class DAO<T> {
    
    protected EntityManager em;
    
    private final Class<T> entidade;
    
    public DAO(Class<T> entidade) {
        this.entidade = entidade;
    }
    
    public void save(T objeto) {
        this.em = JPAUtil.getEntityManager();
        em.getTransaction().begin();
        em.persist(objeto);
        em.getTransaction().commit();
        em.close();
    }
    
    public void update(T objeto) {
        this.em = JPAUtil.getEntityManager();
        em.getTransaction().begin();
        em.merge(objeto);
        em.getTransaction().commit();
        em.close();
        
    }
    
    public void delete(T objeto) {
        this.em = JPAUtil.getEntityManager();
        em.getTransaction().begin();
        em.remove(objeto);
        em.getTransaction().commit();
        em.close();
    }
    
   
    public List<T> findAll(){
        this.em = JPAUtil.getEntityManager();
        List<T> lista ; 
        em.getTransaction().begin();
        Query query = em.createQuery("from " + entidade.getName() +  " c");
        lista = query.getResultList();
        em.getTransaction().commit();  
        em.close();
        return  lista;
    }
    
    
    
    
    
}
