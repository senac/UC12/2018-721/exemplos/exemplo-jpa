package br.com.senac.agenda.dao;

import br.com.senac.agenda.model.Contato;
import java.util.ArrayList;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

public class JPAUtil {

    private static EntityManagerFactory emf
            = Persistence.createEntityManagerFactory("AgendaPU");

    public static EntityManager getEntityManager() {
        try {
            return emf.createEntityManager();
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        return null;
    }

    public static void main(String[] args) {
        EntityManager e = JPAUtil.getEntityManager();
        if (e != null) {
            System.out.println("Conectou ....");
        } else {
            System.out.println("Nao conectou");
        }

        ContatoDAO dao = new ContatoDAO(); 
        List<Contato> contatos = dao.findAll() ;
        for(Contato c : contatos){
            System.out.println(c);
        }
        

    }

}
