package br.com.senac.agenda.dao;

import br.com.senac.agenda.model.Contato;

public class ContatoDAO extends DAO<Contato> {

    public ContatoDAO() {
        super(Contato.class);
    }

}
